from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Snippet
from .serializers import SnippetSerializer, UserSerializer

from django.contrib.auth.models import User

# adding permissions rules for the app
from rest_framework import permissions

# Import the custom permissions 
from .permissions import IsOwnerOrReadOnly

# This views are based on the django class-based views.

class SnippetView(APIView):
    """
    List all snippets from database
    """

    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def post(self, request, format=None):
        serializer = SnippetSerializer(data=request.data)

        if(serializer.is_valid()):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else: 
            return Response(status=status.HTTP_400_BAD_REQUEST)
        
    def get_all_snippets(self, request, format=None):
        snippet = Snippet.objects.all()
        serializer = SnippetSerializer(snippet, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    def put(self, request,pk, format=None):
        snippet = self.get_object(pk)
        serializer = SnippetSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def get_snippet_detail(self, request, pk, format=None):
        snippet = Snippet.get_object(pk)
        serializer = SnippetSerializer(snippet)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, pk, format=None):
        snippet = Snippet.get_object(pk)
        snippet.deletet()
        return Response(status=status.HTTP_200_OK)
    


        

# TODO:  Use mixins here

# TODO: use generic class-based views here.


class UserList(generics.ListAPIView):
    query_set = User.objects.all()
    serializer_class = UserSerializer

    #modify how we can relationships between Creator and Snippet
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class UserDetail(generics.RetrieveAPIView):
    query_set = User.objects.all()
    serializer_class = UserSerializer
    