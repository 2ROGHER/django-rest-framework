from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

# Adding login process to file


# url based functions based views
# urlpatterns = [
#     path('snippets/', views_based_functions.get_all_snippets, name='view'),
#     path('snippets/new', views_based_functions.create_snippet, name='create_snippet'),
#     path('snippets/<int:pk>', views_based_functions.snippet_detail)
# ]

# url class based views
urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path('snippets/', views.APIView.as_view()),
    path('snippets/<int:pk>', views.APIView.as_view()),
    path('users/', views.UserList.as_view()),
    path('users/<int:pk>', views.UserDetail.as_view()),
    
]
# Adding format suffix patterns 
urlpatterns = format_suffix_patterns(urlpatterns)