from rest_framework import serializers
from .models import Snippet


# Ading User model from Auth.contrib.auth.models
from django.contrib.auth.models import User

# DO IT DOES NOT GOO PRACTITE,CAUSE ITS SHWOING MANY INFORMATION


"""
    THIS IS WITH SERIALIZER only clases CLASSES
"""
# class SnippetSerializer(serializers.Serializer):
#     """"
#         Inherits from serializers.Serializer class
#     """    
#     id = serializers.IntegerField(read_only=True)
#     title = serializers.CharField(required=True, allow_blank=True, max_length=100)
#     code = serializers.CharField(style={'base_template':'textarea.html'})
#     linenos = serializers.BooleanField(required=True)
#     language = serializers.ChoiceField(choices=LANGUAGE_CHOICES, default='python')
#     sytle = serializers.ChoiceField(choices=STYLE_CHOICES, default='friendly')

#     def create(self, validated_data):
#         """
#         Create and return a new 'Snippet' instance, given the validate data.
#         """
#         return Snippet.objects.create(**validated_data)
    
#     def update(self, snippet_instance,  validated_data):
#         """
#         Update and return a new 'Snippet' instance, given the the validate data
#         """
#         snippet_instance.title = validated_data.get('title', snippet_instance.title)
#         snippet_instance.code = validated_data.get('code', snippet_instance.code)
#         snippet_instance.linenos = validated_data.get('linenos', snippet_instance.linenos)
#         snippet_instance.language = validated_data.get('language', snippet_instance.language)
#         snippet_instance.style = validated_data.get('style', snippet_instance.style)

#         snippet_instance.save() # save the snippet instance
#         return snippet_instance
    
        
"""
THIS IS WITH ModelSerializer class without Serializer class
"""

class SnippetSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta: 
        model = Snippet
        fields = ['created', 'title', 'code', 'linenos', 'language', 'style', 'owner']


# User serialiazer
class UserSerializer(serializers.ModelSerializer):
    snippet = serializers.PrimaryKeyRelatedField(many=True, queryset=Snippet.objects.all())

    class Meta:
        model = User
        fields=['id', 'username', 'snippet']