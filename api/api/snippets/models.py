from django.db import models

# Create your models here.
from pygments.lexers import get_all_lexers, get_lexer_by_name
from pygments.formatters.html import HtmlFormatter
from pygments import highlight

from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
# STYLE_CHOICES = sorted([(item[1][0], item[0]) for item in get_all_styles()])


class Snippet(models.Model):
    """
    Inherits from Model to create models. All good.
    """
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default="")
    code = models.TextField()
    linenos = models.BooleanField(default=False)
    language = models.CharField(choices=LANGUAGE_CHOICES, default="python", max_length = 100)
    style = models.CharField(default="friendly", max_length = 100)
    owner = models.ForeignKey('auth.User', related_name='snippets', on_delete=models.CASCADE)
    highlighted = models.TextField()

    def save(self, *args, **kwargs):
        """
        User the 'pygments' library to create a highlighted HTML representation
        of the code snippet.
        """
        lexer = get_lexer_by_name(self.language)
        linenos = 'table' if self.linenos else False
        options = {'title': self.title} if self.title else {}
        formatter = HtmlFormatter(style=self.style, liennos=linenos, full=True, **options)

        self.highlighted = highlight(self.code,lexer, formatter)
        super().save(**args, **kwargs)
    
    class Meta:
        db_table = 'snippets'
        ordering = ['created']


